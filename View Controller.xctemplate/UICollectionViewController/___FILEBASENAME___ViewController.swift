//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol ___VARIABLE_sceneName___View: class {
}

final class ___VARIABLE_sceneName___ViewController: UICollectionViewController, ___VARIABLE_sceneName___View {
    
    var presenter: ___VARIABLE_sceneName___PresentationLogic?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
