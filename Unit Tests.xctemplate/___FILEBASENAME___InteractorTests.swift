//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import ___PROJECTNAMEASIDENTIFIER___
import XCTest

final class ___VARIABLE_sceneName___InteractorTests: XCTestCase {
    // MARK: Subject under test
    
    private var sut: ___VARIABLE_sceneName___Interactor!
    
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        sut = ___VARIABLE_sceneName___Interactor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Test doubles
    
    private class ___VARIABLE_sceneName___PresenterSpy: ___VARIABLE_sceneName___PresentationLogic {
        var presentSomethingCalled = false
        
        func presentSomething(response: ___VARIABLE_sceneName___.Something.Response) {
            presentSomethingCalled = true
        }
    }
  
    // MARK: Tests
    
    func testDoSomething() {
        // Given
        let spy = ___VARIABLE_sceneName___PresenterSpy()
        sut.presenter = spy
        let request = ___VARIABLE_sceneName___.Something.Request()
        
        // When
        sut.doSomething(request: request)
        
        // Then
        XCTAssertTrue(spy.presentSomethingCalled, "doSomething(request:) should ask the presenter to format the result")
    }
}
