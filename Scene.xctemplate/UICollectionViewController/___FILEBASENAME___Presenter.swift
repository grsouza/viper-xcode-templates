//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

protocol ___VARIABLE_sceneName___PresentationLogic {
}

final class ___VARIABLE_sceneName___Presenter: ___VARIABLE_sceneName___PresentationLogic {
    weak var view: ___VARIABLE_sceneName___View!
    var interactor: ___VARIABLE_sceneName___BusinessLogic!
    var router: ___VARIABLE_sceneName___RoutingLogic!
}
